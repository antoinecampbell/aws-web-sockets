let connection;

document.addEventListener('DOMContentLoaded', () => {
    document.querySelector('#connect-button').addEventListener('click', () => {
        connection = new WebSocket('wss://rchzepkvv3.execute-api.us-east-1.amazonaws.com/prod');

        connection.onerror = error => {
            console.error(error);
        };

        connection.onmessage = event => {
            console.log(JSON.parse(event.data));
            console.log(event);
        };

        connection.onclose = event => {
            console.log(event);
            connection = null;
        };
    });

    document.querySelector('#send-button').addEventListener('click', () => {
        if (connection) {
            connection.send(JSON.stringify({
                message: document.querySelector('#message').value,
                action: 'echo'
            }));
        } else {
            console.error('No connection')
        }
    });

    document.querySelector('#disconnect-button').addEventListener('click', () => {
        connection.close();
    });
});
