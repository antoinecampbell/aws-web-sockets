provider "aws" {
  version = "~> 2.0"
  region = "us-east-1"
}

data "archive_file" "echo_lambda" {
  type = "zip"
  source_dir = "${path.module}/echo-lambda"
  output_path = "${path.module}/build/echo-lambda.zip"
}

data "archive_file" "push_lambda" {
  type = "zip"
  source_dir = "${path.module}/push-lambda"
  output_path = "${path.module}/build/push-lambda.zip"
}

resource "aws_api_gateway" "push_api_gateway" {
  name = "web-socket-push"
}

# Create Websocket API Gateway, no terraform resource yet
resource "null_resource" "websocket_api_gateway" {
  provisioner "local-exec" {
    command = "aws apigatewayv2 create-api --name web-sockets-connect --protocol-type WEBSOCKET --route-selection-expression '$request.body.action'"
  }
}

