const AWS = require('aws-sdk');
// apply the patch
require('./patch.js');

let send = undefined;
function init() {
    const apigwManagementApi = new AWS.ApiGatewayManagementApi({
        apiVersion: '2018-11-29',
        endpoint: process.env.URL
    });
    send = async (connectionId, data) => {
        await apigwManagementApi.postToConnection({ ConnectionId: connectionId, Data: data }).promise();
    };
}

exports.handler = async (event) => {
    init();
    console.log(event);
    await send(event.connectionId, event.body);
    return {};
};